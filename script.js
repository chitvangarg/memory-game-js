const gameContainer = document.getElementById("game")
const start = document.getElementById("start")
const resetButton = document.getElementById("reset")
const win = document.getElementById("win")
const curr_score = document.getElementById("clicks")
const bestScore = document.getElementById("best")
const easyLevel = document.getElementById("easy")
const mediumLevel = document.getElementById("medium")
const hardLevel = document.getElementById("hard")
const invalid = document.getElementById("invalid")

let numberOfCards

function setLevel(cards) {
    numberOfCards = cards
}

easyLevel.addEventListener("click", function (e) {
    e.preventDefault()
    setLevel(3)
    invalid.style.display = "none"
})

mediumLevel.addEventListener("click", function (e) {
    e.preventDefault()
    setLevel(5)
    invalid.style.display = "none"
})

hardLevel.addEventListener("click", function (e) {
    e.preventDefault()
    setLevel(10)
    invalid.style.display = "none"
})

// easyLevel.style.backgroundColor= 'red';
// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length
    console.log(counter)

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter)
        console.log(index)
        // Decrease counter by 1
        counter--
        console.log(counter)

        // And swap the last element with it
        let temp = array[counter]
        console.log(temp)
        array[counter] = array[index]
        array[index] = temp
    }

    return array
}

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForImages() {
    let images = []

    if (numberOfCards !== undefined && numberOfCards !== "") {
        for (let card = 1; card <= parseInt(numberOfCards); card++) {
            images.push(card)
        }
    }

    images = images.concat(images)

    const shuffledImages = shuffle(images)

    const handleCardClick = handleCardClickClosure(shuffledImages)

    for (let image = 0; image < shuffledImages.length; image++) {
        // create a new div
        const newDiv = document.createElement("div")

        // give it a class attribute for the value we are looping over
        newDiv.classList.add(shuffledImages[image])

        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleCardClick)

        // append the div to the element with an id of game
        gameContainer.append(newDiv)
    }
}

function handleCardClickClosure(images) {
    let score = calculateScore()

    // TODO: Implement this function!
    return function handleCardClick(event) {
        // you can use event.target to see which element was clicked
        const clickedBox = event.target
        clickedBox.classList.add("boxOpen")

        score()

        const openCards = document.querySelectorAll(".boxOpen")

        if (openCards.length <= 2) {
            const cardClasses = clickedBox.classList
            const image = cardClasses[0].toString()
            addImage(clickedBox, image)

            if (openCards.length === 2) {
                if (
                    openCards[0].style.backgroundImage.toString() ==
                    openCards[1].style.backgroundImage.toString()
                ) {
                    openCards[1].classList.add("cardMatched")
                    openCards[0].classList.add("cardMatched")

                    if (
                        document.getElementsByClassName("cardMatched")
                            .length === images.length
                    ) {
                        win.textContent = "You won !"

                        if (numberOfCards === 3) {
                            // Adding best scores
                            if (localStorage.getItem("easy_score") === null) {
                                localStorage.setItem(
                                    "easy_score",
                                    curr_score.textContent
                                )
                            } else if (
                                localStorage.getItem("easy_score") >
                                parseInt(curr_score.textContent)
                            ) {
                                localStorage.setItem(
                                    "easy_score",
                                    curr_score.textContent
                                )
                            }
                            bestScore.innerText =
                                localStorage.getItem("easy_score")
                        } else if (numberOfCards === 5) {
                            // Adding best scores
                            if (localStorage.getItem("medium_score") === null) {
                                localStorage.setItem(
                                    "medium_score",
                                    curr_score.textContent
                                )
                            } else if (
                                localStorage.getItem("medium_score") >
                                parseInt(curr_score.textContent)
                            ) {
                                localStorage.setItem(
                                    "medium_score",
                                    curr_score.textContent
                                )
                            }
                            bestScore.innerText =
                                localStorage.getItem("medium_score")
                        } else {
                            // Adding best scores
                            if (localStorage.getItem("hard_score") === null) {
                                localStorage.setItem(
                                    "hard_score",
                                    curr_score.textContent
                                )
                            } else if (
                                localStorage.getItem("hard_score") >
                                parseInt(curr_score.textContent)
                            ) {
                                localStorage.setItem(
                                    "hard_score",
                                    curr_score.textContent
                                )
                            }
                            bestScore.innerText =
                                localStorage.getItem("hard_score")
                        }
                        resetButton.style.display = "block"
                    }
                    openCards[1].removeEventListener("click", handleCardClick)
                    openCards[0].removeEventListener("click", handleCardClick)

                    closeCards(openCards)
                } else {
                    setTimeout(() => {
                        getBackToOriginal(openCards[0])
                        getBackToOriginal(openCards[1])
                        closeCards(openCards)
                    }, 1000)
                }
            }
        }
    }
}

function addImage(card, image) {
    card.classList.add("rotate-card")
    card.style.backgroundImage = `url("gifs/${image}.gif")`
    card.style.backgroundSize = `100% 100%`
}

function getBackToOriginal(card) {
    card.style.backgroundImage = "url('../image/parachute.png')"
    card.style.backgroundSize = `70% 70%`
}

function closeCards(cards) {
    for (let card = 0; card < cards.length; card++) {
        cards[card].classList.remove("boxOpen")
    }
}

// reset game
resetButton.addEventListener("click", function (event) {
    win.textContent = ""

    const warning = document.getElementById("warning")
    warning.style.display = "flex"

    const yes = document.getElementById("yes")
    const no = document.getElementById("no")

    yes.addEventListener("click", function (e) {
        e.preventDefault()
        e.stopPropagation()
        while (gameContainer.firstChild) {
            gameContainer.removeChild(gameContainer.firstChild)
        }

        warning.style.display = "none"

        createDivsForImages()
    })

    no.addEventListener("click", function (e) {
        e.preventDefault()
        e.stopPropagation()

        warning.style.display = "none"
    })
})

// click calculation
function calculateScore() {
    let clickCount = 0
    const counter = document.getElementById("clicks")
    counter.textContent = clickCount.toString()

    return function () {
        clickCount++
        counter.textContent = clickCount.toString()
    }
}

// start game

start.addEventListener("click", function (e) {
    e.preventDefault()

    if (numberOfCards !== undefined) {
        const container = document.getElementById("game_container")
        const instructions = document.getElementById("instructions")
        const header = document.querySelector("header")
        const scores = document.querySelector("#scores")

        header.style.display = "none"
        container.style.display = "flex"
        gameContainer.style.display = "flex"
        scores.style.display = "flex"

        start.style.display = "none"

        instructions.style.display = "none"

        resetButton.style.display = "flex"

        createDivsForImages()
    } else {
        invalid.textContent = "Please select some level"
    }
})

// when the DOM loads
window.addEventListener("load", function () {
    localStorage.removeItem("easy_score")
    localStorage.removeItem("medium_score")
    localStorage.removeItem("hard_score")
})
